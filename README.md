# Train a Machine Learning Model Using KNN and Logistic Regression

This project aims to classify Iris virginica among three species (setosa, versicolor or virginica) from measurements of length and width of sepals and petals. We use both KNN and Logistic Regression to train ML models.

We use 70 percents of Iris dataset for training model and 30 percents for validating.

For evaluating the model, We calculate confusion matrix for both training and validation sets.

![Confusion Matrix](https://miro.medium.com/max/712/1*Z54JgbS4DUwWSknhDCvNTQ.png)

We use this matrix in order to calculate accuracy. The term accuracy indicates how precise our model is.

There are sevral methods to calculate accuracy; Here we use *Accuracy = (TP + TN) / (TP + FP + FN + TN)*.

### Prerequisites

> [TensorFlow](https://www.tensorflow.org/install)
