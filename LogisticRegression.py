import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets
import tensorflow as tf

iris = datasets.load_iris()
x_vals = np.array([x[0:4] for x in iris.data])

targets = []
for target in iris.target:
    if target == 2:
        targets.append(0)
    else:
        targets.append(1)

y_vals = np.array(targets)


x_vals = (x_vals - x_vals.min(0)) / x_vals.ptp(0)

np.random.seed(59)
train_indices = np.random.choice(len(x_vals), round(len(x_vals) * 0.7), replace=False)
test_indices = np.array(list(set(range(len(x_vals))) - set(train_indices)))

x_train = x_vals[train_indices]
x_train = np.reshape(x_train, [105,4])

x_test = x_vals[test_indices]
x_test = np.reshape(x_test, [45,4])

y_train = y_vals[train_indices]
y_train = np.reshape(y_train, [105,1])

y_test = y_vals[test_indices]
y_test = np.reshape(y_test, [45,1])

test = []
for y_val in y_test:
    test.append(np.argmax(y_val))

feature_number = len(x_train[0])
learning_rate = 0.04
training_epochs = 1000

tf.reset_default_graph()

X = tf.placeholder(shape=[None, 4], dtype=tf.float32)
Y = tf.placeholder(shape=[None, 1], dtype=tf.float32)

W = tf.get_variable("W", [feature_number, 1], initializer=tf.contrib.layers.xavier_initializer())
b = tf.get_variable("b", [1], initializer=tf.zeros_initializer())

Z = tf.add(tf.matmul(X, W), b)
prediction = tf.nn.sigmoid(Z)

cost = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=Z, labels=Y))

optimizer = tf.train.AdamOptimizer(learning_rate).minimize(cost)

init = tf.global_variables_initializer()

cost_history = np.empty(shape=[1], dtype=float)

with tf.Session() as sess:
    sess.run(init)

    for epoch in range(training_epochs):
        _, c = sess.run([optimizer, cost], feed_dict={X: x_train, Y: y_train})
        cost_history = np.append(cost_history, c)

    correct_prediction = tf.to_float(tf.greater(prediction, 0.5))

    def calculate_accuracy(features, labels):
        predictions = correct_prediction.eval({X: features, Y: labels})
        predictions = predictions.transpose()
        predictions = predictions.astype(int)
        labels = labels.transpose()
        confusion_matrix = tf.confusion_matrix(predictions[0], labels[0], num_classes= 2, dtype=tf.int32)
        with tf.Session():
            cMat = tf.Tensor.eval(confusion_matrix, feed_dict= None, session= None)
        print(cMat)
        tp, tn, fp, fn = cMat[0,0], cMat[1,1], cMat[1,0], cMat[0,1]
        accuracy = (tp + tn) / (tp + tn + fp + fn)
        return accuracy

    print("Train Accuracy: ", calculate_accuracy(x_train, y_train))
    print("Train Accuracy: ", calculate_accuracy(x_test, y_test))
